from django.test import TestCase
from django.test import Client
from .models import Contenido, Comentario
from django.contrib.auth.models import User
from django.contrib.auth import authenticate


class Test(TestCase):
    def testing_logout(self):
        client = Client()
        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        client.login(username='admin1', password='admin1')
        response = client.post('/', {'action': "Logout"})
        self.assertEqual(response.status_code, 302)

    def testing_login_valid(self):
        client = Client()
        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        test = client.login(username='admin1', password='admin1')
        self.assertIn(str(test), 'True')

    def testing_login_error(self):
        client = Client()
        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        test = client.login(username='admin1', password='administrador2')
        self.assertIn(str(test), 'False')

    def testing_get_index(self):
        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        Contenido(url="http://www.marca.com", html="", titulo="Titulo del marca",
                descripcion="descripcion del marca", user=User.objects.get(username="admin1")).save()
        client = Client()
        response = client.get('')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>' + "Título: Titulo del marca" + '</p>', content)
        self.assertIn('<p>' + "Descripción: descripcion del marca" + '</p>', content)

    def testing_post_content(self):
        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        titulo = "Primer video youtube"
        descripcion = "Primer video"
        client = Client()
        client.login(username="admin1", password="admin1")
        response = client.post('', {'action': "Añadir aportación", 'url': url,
                               'titulo': titulo, 'descripcion': descripcion})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>' + "Título: " + titulo + '</p>', content)
        self.assertIn('<p>' + "Descripción: " +
                      descripcion + '</p>', content)

    def test_post_comentario(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        titulo = "Primer video youtube"
        descripcion = "Primer video"
        titulo_comentario = "Este es el titulo"
        cuerpo = "Este es el cuerpo"
        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        client = Client()
        client.login(username="admin1", password="admin1")

        client.post('', {'action': "Añadir aportación", 'url': url,
                    'titulo': titulo, 'descripcion': descripcion})
        Comentario(contenido=Contenido.objects.get(id=1), user_name=User.objects.get(username="admin1"),
                   user_id=User.objects.get(username="admin1").id, titulo=titulo_comentario, cuerpo=cuerpo).save()

        response = client.post('/youtube/IfoSqaxJsAM', {'action': 'Añadir comentario', 'titulo': titulo,
                                                              'cuerpo': cuerpo, 'url_imagen': ''})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<li>Title: '+titulo+'</li>', content)

    def test_aportacion_page(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        titulo = "Primer video youtube"
        descripcion = "Primer video"

        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        client = Client()
        client.login(username="admin1", password="admin1")

        client.post('', {'action': "Añadir aportación", 'url': url,
                    'titulo': titulo, 'descripcion': descripcion})
        response = client.get('/youtube/IfoSqaxJsAM')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>Título: '+titulo+'</p>', content)

    def test_aportacion_page_error(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        titulo = "Primer video youtube"
        descripcion = "Primer video"

        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        client = Client()
        client.login(username="admin1", password="admin1")

        client.post('', {'action': "Añadir aportación", 'url': url,
                    'titulo': titulo, 'descripcion': descripcion})
        response = client.get('/youtube/66gge5')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn("No existe esta aportación. Introduce una url correcta", content)

    def test_user_page(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        titulo = "Primer video youtube"
        descripcion = "Primer video"

        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        client = Client()
        client.login(username="admin1", password="admin1")

        client.post('', {'action': "Añadir aportación", 'url': url,
                    'titulo': titulo, 'descripcion': descripcion})
        contenido = Contenido.objects.get(titulo=titulo)
        response = client.get('/user')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<li> '+contenido.html_resumido+ ' </li>', content)

    def test_aportaciones_page(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        titulo = "Primer video youtube"
        descripcion = "Primer video"

        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        client = Client()
        client.login(username="admin1", password="admin1")

        client.post('', {'action': "Añadir aportación", 'url': url,
                    'titulo': titulo, 'descripcion': descripcion})
        contenido = Contenido.objects.get(titulo=titulo)
        response = client.get('/aportaciones')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<li>'+contenido.html_resumido+ '</li>', content)

    def test_xml(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        titulo = "Primer video youtube"
        descripcion = "Primer video"
        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        client = Client()
        client.login(username="admin1", password="admin1")

        client.post('', {'action': "Añadir aportación", 'url': url,
                         'titulo': titulo, 'descripcion': descripcion})
        contenido = Contenido.objects.get(titulo=titulo)
        response = client.get('/aportaciones?format=xml')
        content = response.content.decode('utf-8')
        self.assertIn("<aportaciones>", content)
        self.assertIn(contenido.titulo , content)
        self.assertIn(contenido.url_path, content)

    def test_json(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        titulo = "Primer video youtube"
        descripcion = "Primer video"
        User.objects.create_user("admin1", 'admin1@urjc.com', "admin1").save()
        client = Client()
        client.login(username="admin1", password="admin1")

        client.post('', {'action': "Añadir aportación", 'url': url,
                         'titulo': titulo, 'descripcion': descripcion})
        contenido = Contenido.objects.get(titulo=titulo)
        response = client.get('/aportaciones?format=json')
        content = response.content.decode('utf-8')
        self.assertIn(contenido.titulo, content)
        self.assertIn(contenido.url_path, content)


