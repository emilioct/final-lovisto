import urllib

from django.contrib.auth.forms import AuthenticationForm
from lxml import html, etree

from .models import Contenido, Comentario, Modo
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout, authenticate, login
from django.shortcuts import redirect, render
from django.template import loader
#INDICAR QUE SE HA USADO LA LIBRERIA XML TO DICT
import xmltodict
import json
from bs4 import BeautifulSoup
from .forms import ComentarioForm


form_logout = """
Hey! You are authenticated as '{{user}}'.
<form action="" method="POST">
    <br/input type="submit" value="Logout">
</form>
"""

no_usuario = False
menu_links = []


def links(request, string):
    global menu_links
    menu_links = []
    if string == "index":
        link = "<a href='" + "/aportaciones" + "'>" + "Todo" + "</a>"
        menu_links.append(link)
        if request.user.is_authenticated:
            link = "<a href='" + "/user" + "'>" + "User" + "</a>"
            menu_links.append(link)
    elif string == "user":
        link = "<a href='" + "/" + "'>" + "Inicio" + "</a>"
        menu_links.append(link)
        link = "<a href='" + "/aportaciones" + "'>" + "Todo" + "</a>"
        menu_links.append(link)
    elif string == "aportacion":
        link = "<a href='" + "/" + "'>" + "Inicio" + "</a>"
        menu_links.append(link)
        link = "<a href='" + "/aportaciones" + "'>" + "Todo" + "</a>"
        menu_links.append(link)
        if request.user.is_authenticated:
            link = "<a href='" + "/user" + "'>" + "User" + "</a>"
            menu_links.append(link)
    elif string == 'aportaciones':
        link = "<a href='" + "/" + "'>" + "Inicio" + "</a>"
        menu_links.append(link)
        if request.user.is_authenticated:
            link = "<a href='" + "/user" + "'>" + "User" + "</a>"
            menu_links.append(link)

    link = "<a href='" + "/aportaciones?format=xml" + "'>" + "Descargar aportaciones como fichero XML" + "</a>"
    menu_links.append(link)
    link = "<a href='" + "/aportaciones?format=json" + "'>" + "Descargar aportaciones como fichero JSON" + "</a>"
    menu_links.append(link)
    link = "<a href='" + "/comentarios?format=xml" + "'>" + "Descargar comentarios como fichero XML" + "</a>"
    menu_links.append(link)
    link = "<a href='" + "/comentarios?format=json" + "'>" + "Descargar comentarios como fichero JSON" + "</a>"
    menu_links.append(link)
    link = "<a href='" + "/informacion" + "'>" + "Información de la aplicación" + "</a>"
    menu_links.append(link)

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    if request.user.is_authenticated:
        if oscuro:
            link = "<a class ='active' href='" + "/modo_normal" + "'>" + "Modo Normal" + "</a>"
            menu_links.append(link)
        else:
            link = "<a class ='active' href='" + "/modo_oscuro" + "'>" + "Modo Oscuro" + "</a>"
            menu_links.append(link)


def parse_recurso(request, recurso):
    url = request.POST['url']
    titulo = request.POST['titulo']
    descripcion = request.POST['descripcion']
    if "/es/eltiempo/prediccion/municipios/" in recurso:
        recurso = recurso.split('/')[5]
        municipio, id_municipio = recurso.split('-id')
        url_path = "/aemet/" + municipio + "-id" + id_municipio
        xml_url = "https://www.aemet.es/xml/municipios/localidad_" + id_municipio + ".xml"
        xml = urllib.request.urlopen(xml_url)
        xmldict = xmltodict.parse(xml.read())
        provincia = xmldict["root"]["provincia"]
        prediccion = []
        for dia in xmldict["root"]["prediccion"]["dia"]:
            fecha = dia["@fecha"]
            temperatura = dia["temperatura"]["maxima"] + "/" + dia["temperatura"]["minima"]
            sensacion = dia["sens_termica"]["maxima"] + "/" + dia["sens_termica"]["minima"]
            humedad = dia["humedad_relativa"]["maxima"] + "/" + dia["humedad_relativa"]["minima"]
            dia = (fecha + ". Temperatura: " + temperatura + ", sensación: "
                   + sensacion + ", humedad: " + humedad)
            prediccion.append(dia)
            prediccion_html = ""
            for p in prediccion:
                d = "<li>" + p + "</li>"
                prediccion_html = prediccion_html + d
        print(url)
        html = ("<div class='aemet'>" +
                "<p>Datos AEMET para " + municipio.title() + " (" + provincia + ")</p>" +
                prediccion_html +
                "<p>Copyright AEMET. <a href='" + url + "'> Página original en AEMET </a></p>" +
                "</div>"
                )
    elif "/wiki/" in recurso:
        articulo = recurso.split('/')[2]
        url_path = "/wiki/" + articulo
        xml_url = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles="+articulo+"&prop=extracts&exintro&explaintext"
        json_url = "https://es.wikipedia.org/w/api.php?action=query&titles="+articulo+"&prop=pageimages&format=json&pithumbsize=100"
        xml = urllib.request.urlopen(xml_url)
        xmldict = xmltodict.parse(xml.read())
        json_file = urllib.request.urlopen(json_url)
        json_dict = json.loads(json_file.read().decode('utf8'))
        texto_articulo = xmldict["api"]["query"]["pages"]["page"]["extract"]['#text']
        id_json = str(xmldict["api"]["query"]["pages"]["page"]["@pageid"])
        if len(texto_articulo) > 400:
            texto_articulo = texto_articulo[0:400]
        imagen = json_dict["query"]["pages"][id_json]["thumbnail"]["source"]
        html = (
                "<div class="'wikipedia'">" +
                "<p>Artíıculo Wikipedia: "+articulo+"</p>" +
                "<img src='"+imagen+"' width='150' height='150'>" +
                "<p>"+texto_articulo+"</p>" +
                "<p>Copyright Wikipedia. " +
                "<a href='https://es.wikipedia.org/wiki/"+articulo+"'>Artículo original</a></p>"+
                "</div>"
        )
    elif "/watch?v=" in recurso:
        video = recurso.split('watch?v=')[1]
        url_path = "/youtube/" + video
        json_url = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v="+video
        json_file = urllib.request.urlopen(json_url)
        json_dict = json.loads(json_file.read().decode('utf8'))
        title = json_dict["title"]
        autor = json_dict["author_name"]
        video_embed = json_dict["html"]
        html = (
                "<div class='youtube'>" +
                "<p>Video YouTube: " + title + "</p>" +
                video_embed +
                "<p>Autor: " + autor + ". " +
                "<a href='"+url+"'>Video en YouTube</a></p>" +
                "</div>"
        )
    elif ("/r/" in recurso) and ("/comments/" in recurso):
        recurso = recurso.split("/")
        subreddit = recurso[2]
        id_reddit = recurso[4]
        title = recurso[5]
        url_path ="/reddit/"+id_reddit
        json_url = ("https://www.reddit.com/r/"+subreddit+"/comments/"+id_reddit+"/.json")
        json_file = urllib.request.urlopen(json_url)
        json_dict = json.loads(json_file.read().decode('utf8'))
        subreddit = json_dict[0]["data"]["children"][0]["data"]["subreddit"]
        title_reddit = json_dict[0]["data"]["children"][0]["data"]["title"]
        text_reddit = json_dict[0]["data"]["children"][0]["data"]["selftext"]
        aprobacion = json_dict[0]["data"]["children"][0]["data"]["upvote_ratio"]
        url_reddit = json_dict[0]["data"]["children"][0]["data"]["url"]
        if "i.redd.it" in url_reddit:
            html=("<div class='reddit'>"+
                "<p>Nota Reddit: " + title_reddit + "</p>"+
                "<img src='"+url_reddit+"' width='150' height='150'>"+
                "<p><a href='"+url+"'>Publicado "+
                "en "+ subreddit+"</a>, Aprobacion: "+str(aprobacion)+".</p>"
                "</div>")
        else:
            html=("<div class='reddit'>"+
                "<p>Nota Reddit: " + title_reddit + "</p>"+
                "<p>"+text_reddit+"</p>"+
                "<p><a href='"+url_reddit+"'>Publicado "+
                "en "+ subreddit+"</a>, Aprobacion: "+str(aprobacion)+".</p>"
                "</div>")
    else:
        req = urllib.request.urlopen(url)
        soup = BeautifulSoup(req, 'html.parser')
        ogTitle = soup.find('meta', property='og:title')
        if ogTitle:
            title = ogTitle['content']
        ogImage = soup.find('meta', property='og:image')
        if ogImage:
            imagen = ogImage['content']

        if ogTitle and ogImage:
            html= ("<div class ='og'>" +
            "<p>" + title + "</p>" +
            "<img src = '" + imagen + "'>" +
            "</div>")
        elif ogTitle and not ogImage:
            html = ("<div class ='og'>" +
                    "<p>" + title + "</p>" +
                    "</div>")
        elif ogImage and not ogTitle:
            html = ("<div class ='og'>" +
                    "<p>" + titulo + "</p>" +
                    "<img src = '" + imagen + "'>" +
                    "</div>")
        else:
            html = ("<div class ='og'>" +
            "<p> Información extendida no disponible </p>"+
            "</div>")

        url_path = "/no_reconocido/" + titulo

    html_resumido = (
            "<a href='" + url_path + "'>" + titulo + "</a>"
    )
    content = Contenido(url=url, titulo=titulo, url_path=url_path, descripcion=descripcion,
                        html=html, html_resumido=html_resumido, tipo="no_reconocido", user=request.user)
    content.save()

    if request.user.is_authenticated:
        form_login = form_logout
        authenticated = True
    else:
        form_login = AuthenticationForm()
        authenticated = False

    user = request.user
    content_list = Contenido.objects.all()
    if len(content_list) > 10:
        content_list = list(content_list)
        content_list = content_list[::-1]
        content_list = content_list[0:10]
        content_list = content_list[::-1]

    enlace_list = Contenido.objects.all()
    enlace_list = list(enlace_list)
    enlace_list = enlace_list[::-1]
    enlace_list = enlace_list[0:3]
    enlace_list = enlace_list[::-1]
    enlaces = "Esta aplicación enlaza a: "
    for e in enlace_list:
        enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
        enlaces = enlaces + enl + ", "
    enlaces = enlaces + "y otros muchos sitios"

    content_user_list = Contenido.objects.filter(user=user)
    if len(content_user_list) > 5:
        content_user_list = list(content_user_list)
        content_user_list = content_user_list[::-1]
        content_user_list = content_user_list[0:5]
        content_user_list = content_user_list[::-1]
    content_user_list_exist = True

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    links(request, "index")
    global menu_links

    context = {'content_list': content_list, 'enlaces': enlaces, 'menu_links': menu_links,
               'form_login': form_login, 'is_authenticated': authenticated, 'modo_oscuro': oscuro,
               'user': user, 'content_user_list_exist': content_user_list_exist, 'content_user_list': content_user_list}
    template = loader.get_template('lovisto/index.html')
    return HttpResponse(template.render(context, request))


def like_content(request):
    content_id = request.POST['content_id']
    content = Contenido.objects.get(id=content_id)
    content.likes.add(request.user)
    content.dislikes.remove(request.user)
    return redirect('/')


def dislike_content(request):
    content_id = request.POST['content_id']
    content = Contenido.objects.get(id=content_id)
    content.likes.remove(request.user)
    content.dislikes.add(request.user)
    return redirect('/')


def like_aportacion(request):
    content_id = request.POST['content_id']
    content = Contenido.objects.get(id=content_id)
    content.likes.add(request.user)
    content.dislikes.remove(request.user)
    return redirect(content.url_path)


def dislike_aportacion(request):
    content_id = request.POST['content_id']
    content = Contenido.objects.get(id=content_id)
    content.likes.remove(request.user)
    content.dislikes.add(request.user)
    return redirect(content.url_path)


def mi_login(request):
    user = request.POST['user']
    user_password = request.POST['password']
    acceso = authenticate(username=user, password=user_password)
    global no_usuario
    if acceso is None:
        no_usuario = True
    else:
        login(request, acceso)
        no_usuario = False

    return redirect('/')


@csrf_exempt
def index(request):
    method = request.method
    if method == "POST":
        action = request.POST['action']
        if action == "Añadir aportación":
            url = request.POST['url']
            if ('https://www.aemet.es' in url) or ('https://aemet.es' in url) or \
                    ('http://www.aemet.es' in url) or ('http://aemet.es' in url):
               recurso = url.split('aemet.es')[1]
               parse_recurso(request, recurso)
            elif ('https://es.wikipedia.org' in url) or ('http://es.wikipedia.org' in url):
                recurso = url.split('es.wikipedia.org')[1]
                parse_recurso(request, recurso)
            elif ('https://www.youtube.com' in url) or ('https://youtube.com' in url) or \
                    ('http://www.youtube.com' in url) or ('http://youtube.com' in url):
                recurso = url.split('youtube.com')[1]
                parse_recurso(request, recurso)
            elif ('https://www.reddit.com' in url) or ('https://reddit.com' in url) or \
                    ('http://www.reddit.com' in url) or ('http://reddit.com' in url):
                recurso = url.split('reddit.com')[1]
                parse_recurso(request, recurso)
            else:
                recurso = ""
                parse_recurso(request, recurso)
        if action == "Login":
            mi_login(request)
            return redirect('/')
        if action == "Logout":
            logout(request)
            return redirect("/")
    if request.user.is_authenticated:
        form_login = form_logout
        authenticated = True
    else:
        form_login = AuthenticationForm()
        authenticated = False

    user = request.user
    content_list = Contenido.objects.all()
    if len(content_list) > 10:
        content_list = list(content_list)
        content_list = content_list[::-1]
        content_list = content_list[0:10]
        content_list = content_list[::-1]

    enlace_list = Contenido.objects.all()
    enlace_list = list(enlace_list)
    enlace_list = enlace_list[::-1]
    enlace_list = enlace_list[0:3]
    enlace_list = enlace_list[::-1]
    enlaces = "Esta aplicación enlaza a: "
    for e in enlace_list:
        enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
        enlaces = enlaces + enl + ", "
    enlaces = enlaces + "y otros muchos sitios"

    try:
        content_user_list = Contenido.objects.filter(user=user)
        if len(content_user_list) > 5:
            content_user_list = list(content_user_list)
            content_user_list = content_user_list[::-1]
            content_user_list = content_user_list[0:5]
            content_user_list = content_user_list[::-1]
        content_user_list_exist = True
    except Contenido.DoesNotExist:
        content_user_list = ""
        content_user_list_exist = False
    for content in content_list:
        if content.likes.filter(id=request.user.id).exists():
            content.is_liked = True
        else:
            content.is_liked = False
        if content.dislikes.filter(id=request.user.id).exists():
            content.is_disliked = True
        else:
            content.is_disliked = False

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    global no_usuario

    links(request, "index")
    global menu_links

    context = {'content_list': content_list, 'enlaces': enlaces, 'modo_oscuro': oscuro, 'menu_links': menu_links,
               'form_login': form_login, 'is_authenticated': authenticated, 'user': user, 'no_usuario': no_usuario,
               'content_user_list_exist': content_user_list_exist, 'content_user_list': content_user_list}
    template = loader.get_template('lovisto/index.html')
    if request.GET.get('format') == 'xml':
        return render(request, 'lovisto/xml/index.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'lovisto/json/index.json', context, content_type="text/json")
    else:
        return HttpResponse(template.render(context, request))


def aportaciones(request):
    content_list = Contenido.objects.all()
    content_list = list(content_list)
    content_list = content_list[::-1]

    enlace_list = Contenido.objects.all()
    enlace_list = list(enlace_list)
    enlace_list = enlace_list[::-1]
    enlace_list = enlace_list[0:3]
    enlace_list = enlace_list[::-1]
    enlaces = "Esta aplicación enlaza a: "
    for e in enlace_list:
        enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
        enlaces = enlaces + enl + ", "
    enlaces = enlaces + "y otros muchos sitios"

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    links(request, "aportaciones")
    global menu_links

    context = {'content_list': content_list, 'enlaces': enlaces, 'modo_oscuro': oscuro,
               'menu_links': menu_links}
    template = loader.get_template('lovisto/aportaciones.html')
    if request.GET.get('format') == 'xml':
        return render(request, 'lovisto/xml/aportaciones.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'lovisto/json/aportaciones.json', context, content_type="text/json")
    else:
        return HttpResponse(template.render(context, request))


def user_page(request):
    my_user = request.user
    if my_user.is_authenticated:
        try:
            content_user_list = list(Contenido.objects.filter(user=my_user))
        except Contenido.DoesNotExist:
            content_user_list = ""

        votos_positivos_user_list = []
        votos_negativos_user_list = []
        content_list = Contenido.objects.all()
        if content_list:
            for content in content_list:
                if content.likes.filter(id=my_user.id).exists():
                    voto = content.html_resumido
                    votos_positivos_user_list.append(voto)
                if content.dislikes.filter(id=my_user.id).exists():
                    voto = content.html_resumido
                    votos_negativos_user_list.append(voto)

        comentarios = Comentario.objects.filter(user_name=my_user)
        comentarios = comentarios.all()

        enlace_list = Contenido.objects.all()
        enlace_list = list(enlace_list)
        enlace_list = enlace_list[::-1]
        enlace_list = enlace_list[0:3]
        enlace_list = enlace_list[::-1]
        enlaces = "Esta aplicación enlaza a: "
        for e in enlace_list:
            enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
            enlaces = enlaces + enl + ", "
        enlaces = enlaces + "y otros muchos sitios"

        try:
            modo = Modo.objects.get(user=request.user)
            oscuro = modo.modo_oscuro
        except Modo.DoesNotExist:
            oscuro = False

        links(request, "user")
        global menu_links

        context = {'user': my_user, 'content_user_list': content_user_list, 'content_list': content_list,
                   'comment_user_list': comentarios, 'votos_positivos_user_list': votos_positivos_user_list,
                   'votos_negativos_user_list': votos_negativos_user_list, 'enlaces': enlaces,
                   'menu_links': menu_links, 'modo_oscuro': oscuro}
        template = loader.get_template('lovisto/user.html')
        if request.GET.get('format') == 'xml':
            return render(request, 'lovisto/xml/user.xml', context, content_type="text/xml")
        elif request.GET.get('format') == 'json':
            return render(request, 'lovisto/json/user.json', context, content_type="text/json")
        else:
            return HttpResponse(template.render(context, request))
    else:
        response = "No puedes ver esta página porque no estás autenticado, ve a la página principal y logueate"
        return HttpResponse(response)


def aportacion_aemet_page(request, municipio, municipio_id):
    url_path = "/aemet/"+municipio+"-id"+str(municipio_id)
    try:
        content = Contenido.objects.get(url_path=url_path)
    except Contenido.DoesNotExist:
        response = "No existe esta aportación. Introduce una url correcta"
        return HttpResponse(response + url_path)

    if request.method == "POST":
        if request.POST["action"] == "Añadir comentario":
            titulo = request.POST["titulo"]
            cuerpo = request.POST["cuerpo"]
            url_imagen = request.POST["url_imagen"]
            user_name = request.user
            user_id = request.user.id
            c = Comentario(contenido=content, titulo=titulo, cuerpo=cuerpo, image_url=url_imagen,
                           user_name=user_name, user_id=user_id)
            c.save()

    if content.likes.filter(id=request.user.id).exists():
        content.is_liked = True
    else:
        content.is_liked = False
    if content.dislikes.filter(id=request.user.id).exists():
        content.is_disliked = True
    else:
        content.is_disliked = False

    enlace_list = Contenido.objects.all()
    enlace_list = list(enlace_list)
    enlace_list = enlace_list[::-1]
    enlace_list = enlace_list[0:3]
    enlace_list = enlace_list[::-1]
    enlaces = "Esta aplicación enlaza a: "
    for e in enlace_list:
        enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
        enlaces = enlaces + enl + ", "
    enlaces = enlaces + "y otros muchos sitios"

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    links(request, "aportacion")
    global menu_links

    context = {'user': request.user, 'enlaces': enlaces, 'content': content, 'modo_oscuro': oscuro,
               'menu_links': menu_links}
    template = loader.get_template('lovisto/aportacion.html')
    if request.GET.get('format') == 'xml':
        return render(request, 'lovisto/xml/aportacion.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'lovisto/json/aportacion.json', context, content_type="text/json")
    else:
        return HttpResponse(template.render(context, request))


def aportacion_wiki_page(request, articulo):
    url_path = "/wiki/" + articulo
    try:
        content = Contenido.objects.get(url_path=url_path)
    except Contenido.DoesNotExist:
        response = "No existe esta aportación. Introduce una url correcta"
        return HttpResponse(response)

    if request.method == "POST":
        if request.POST["action"] == "Añadir comentario":
            titulo = request.POST["titulo"]
            cuerpo = request.POST["cuerpo"]
            url_imagen = request.POST["url_imagen"]
            user_name = request.user
            user_id = request.user.id
            c = Comentario(contenido=content, titulo=titulo, cuerpo=cuerpo, image_url=url_imagen,
                           user_name=user_name, user_id=user_id)
            c.save()

    if content.likes.filter(id=request.user.id).exists():
        content.is_liked = True
    else:
        content.is_liked = False
    if content.dislikes.filter(id=request.user.id).exists():
        content.is_disliked = True
    else:
        content.is_disliked = False

    enlace_list = Contenido.objects.all()
    enlace_list = list(enlace_list)
    enlace_list = enlace_list[::-1]
    enlace_list = enlace_list[0:3]
    enlace_list = enlace_list[::-1]
    enlaces = "Esta aplicación enlaza a: "
    for e in enlace_list:
        enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
        enlaces = enlaces + enl + ", "
    enlaces = enlaces + "y otros muchos sitios"

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    links(request, "aportacion")
    global menu_links

    context = {'user': request.user, 'enlaces': enlaces, 'content': content, 'modo_oscuro': oscuro,
               'menu_links': menu_links}
    template = loader.get_template('lovisto/aportacion.html')
    if request.GET.get('format') == 'xml':
        return render(request, 'lovisto/xml/aportacion.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'lovisto/json/aportacion.json', context, content_type="text/json")
    else:
        return HttpResponse(template.render(context, request))


def aportacion_youtube_page(request, video):
    url_path = "/youtube/" + video
    try:
        content = Contenido.objects.get(url_path=url_path)
    except Contenido.DoesNotExist:
        response = "No existe esta aportación. Introduce una url correcta"
        return HttpResponse(response)

    if request.method == "POST":
        if request.POST["action"] == "Añadir comentario":
            titulo = request.POST["titulo"]
            cuerpo = request.POST["cuerpo"]
            url_imagen = request.POST["url_imagen"]
            user_name = request.user
            user_id = request.user.id
            c = Comentario(contenido=content, titulo=titulo, cuerpo=cuerpo, image_url=url_imagen,
                           user_name=user_name, user_id=user_id)
            c.save()

    if content.likes.filter(id=request.user.id).exists():
        content.is_liked = True
    else:
        content.is_liked = False
    if content.dislikes.filter(id=request.user.id).exists():
        content.is_disliked = True
    else:
        content.is_disliked = False

    enlace_list = Contenido.objects.all()
    enlace_list = list(enlace_list)
    enlace_list = enlace_list[::-1]
    enlace_list = enlace_list[0:3]
    enlace_list = enlace_list[::-1]
    enlaces = "Esta aplicación enlaza a: "
    for e in enlace_list:
        enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
        enlaces = enlaces + enl + ", "
    enlaces = enlaces + "y otros muchos sitios"

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    links(request, "aportacion")
    global menu_links

    context = {'user': request.user, 'enlaces': enlaces, 'content': content, 'modo_oscuro': oscuro,
               'menu_links': menu_links}
    template = loader.get_template('lovisto/aportacion.html')
    if request.GET.get('format') == 'xml':
        return render(request, 'lovisto/xml/aportacion.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'lovisto/json/aportacion.json', context, content_type="text/json")
    else:
        return HttpResponse(template.render(context, request))


def aportacion_reddit_page(request, id_reddit):
    url_path = "/reddit/" + id_reddit
    try:
        content = Contenido.objects.get(url_path=url_path)
    except Contenido.DoesNotExist:
        response = "No existe esta aportación. Introduce una url correcta"
        return HttpResponse(response)

    if request.method == "POST":
        if request.POST["action"] == "Añadir comentario":
            titulo = request.POST["titulo"]
            cuerpo = request.POST["cuerpo"]
            url_imagen = request.POST["url_imagen"]
            user_name = request.user
            user_id = request.user.id
            c = Comentario(contenido=content, titulo=titulo, cuerpo=cuerpo, image_url=url_imagen,
                           user_name=user_name, user_id=user_id)
            c.save()

    if content.likes.filter(id=request.user.id).exists():
        content.is_liked = True
    else:
        content.is_liked = False
    if content.dislikes.filter(id=request.user.id).exists():
        content.is_disliked = True
    else:
        content.is_disliked = False

    enlace_list = Contenido.objects.all()
    enlace_list = list(enlace_list)
    enlace_list = enlace_list[::-1]
    enlace_list = enlace_list[0:3]
    enlace_list = enlace_list[::-1]
    enlaces = "Esta aplicación enlaza a: "
    for e in enlace_list:
        enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
        enlaces = enlaces + enl + ", "
    enlaces = enlaces + "y otros muchos sitios"

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    links(request, "aportacion")
    global menu_links

    context = {'user': request.user, 'enlaces': enlaces, 'content': content, 'modo_oscuro': oscuro,
               'menu_links': menu_links}
    template = loader.get_template('lovisto/aportacion.html')
    if request.GET.get('format') == 'xml':
        return render(request, 'lovisto/xml/aportacion.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'lovisto/json/aportacion.json', context, content_type="text/json")
    else:
        return HttpResponse(template.render(context, request))


def aportacion_no_reconocido(request, titulo):
    url_path = "/no_reconocido/" + titulo
    try:
        content = Contenido.objects.get(url_path=url_path)
    except Contenido.DoesNotExist:
        response = "No existe esta aportación. Introduce una url correcta"
        return HttpResponse(response)

    if request.method == "POST":
        if request.POST["action"] == "Añadir comentario":
            titulo = request.POST["titulo"]
            cuerpo = request.POST["cuerpo"]
            url_imagen = request.POST["url_imagen"]
            user_name = request.user
            user_id = request.user.id
            c = Comentario(contenido=content, titulo=titulo, cuerpo=cuerpo, image_url=url_imagen,
                           user_name=user_name, user_id=user_id)
            c.save()

    if content.likes.filter(id=request.user.id).exists():
        content.is_liked = True
    else:
        content.is_liked = False
    if content.dislikes.filter(id=request.user.id).exists():
        content.is_disliked = True
    else:
        content.is_disliked = False

    enlace_list = Contenido.objects.all()
    enlace_list = list(enlace_list)
    enlace_list = enlace_list[::-1]
    enlace_list = enlace_list[0:3]
    enlace_list = enlace_list[::-1]
    enlaces = "Esta aplicación enlaza a: "
    for e in enlace_list:
        enl = "<a href='" + e.url + "'>" + e.titulo + "</a>"
        enlaces = enlaces + enl + ", "
    enlaces = enlaces + "y otros muchos sitios"

    try:
        modo = Modo.objects.get(user=request.user)
        oscuro = modo.modo_oscuro
    except Modo.DoesNotExist:
        oscuro = False

    links(request, "aportacion")
    global menu_links

    context = {'user': request.user, 'enlaces': enlaces, 'content': content, 'modo_oscuro': oscuro,
               'menu_links': menu_links}
    template = loader.get_template('lovisto/aportacion.html')
    if request.GET.get('format') == 'xml':
        return render(request, 'lovisto/xml/aportacion.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'lovisto/json/aportacion.json', context, content_type="text/json")
    else:
        return HttpResponse(template.render(context, request))


def modo_oscuro(request):
    try:
        modo_user = Modo.objects.get(user=request.user)
        modo_user.modo_oscuro = True
        modo_user.save()
    except Modo.DoesNotExist:
        m = Modo(user=request.user, modo_oscuro=True)
        m.save()
    return redirect("/")


def modo_normal(request):
    modo_user = Modo.objects.get(user=request.user)
    modo_user.modo_oscuro = False
    modo_user.save()
    return redirect("/")


def tipo_comentarios(request):
    content_list = Contenido.objects.all()
    context = {'content_list': content_list}
    if request.GET.get('format') == 'xml':
        return render(request, 'lovisto/xml/comentarios.xml', context, content_type="text/xml")
    elif request.GET.get('format') == 'json':
        return render(request, 'lovisto/json/comentarios.json', context, content_type="text/json")


def informacion(request):
    context = {}
    template = loader.get_template('lovisto/informacion.html')
    return HttpResponse(template.render(context, request))
