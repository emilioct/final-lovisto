from django.contrib.auth.models import User
from django.db import models


class Contenido(models.Model):
    url = models.CharField(max_length=256)
    titulo = models.CharField(max_length=256)
    url_path = models.CharField(max_length=256, default='')
    descripcion = models.TextField(blank=True)
    html = models.TextField(blank=True)
    html_resumido = models.TextField(blank=True)
    tipo = models.CharField(max_length=256)
    user = models.CharField(max_length=256, default=User)
    fecha = models.DateTimeField(auto_now=True)
    likes = models.ManyToManyField(User, related_name='likes', blank=True)
    is_liked = models.BooleanField
    is_disLiked = models.BooleanField
    dislikes = models.ManyToManyField(User, related_name='dislikes', blank=True)

    def total_likes(self):
        return self.likes.count()

    def total_dislikes(self):
        return self.dislikes.count()

    num_likes = total_likes
    num_dislikes = total_dislikes

    def __str__(self):
        return str(self.titulo) + "--> id =" + str(self.id)


class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=128)
    cuerpo = models.TextField(blank=True)
    fecha = models.DateTimeField(auto_now=True)
    user_id = models.IntegerField(blank=False, default=0)
    user_name = models.CharField(max_length=128, default='')
    image_url = models.CharField(max_length=256, blank=True, null=True)

    def __str__(self):
        return str(self.titulo) + "--> id =" + str(self.id) + "--> contenido =" + str(self.contenido.titulo)


class Modo(models.Model):
    user = models.CharField(max_length=256, default=User)
    modo_oscuro = models.BooleanField()




