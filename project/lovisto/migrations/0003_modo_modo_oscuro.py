# Generated by Django 3.1.7 on 2021-05-29 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lovisto', '0002_modo'),
    ]

    operations = [
        migrations.AddField(
            model_name='modo',
            name='modo_oscuro',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
