from django.shortcuts import redirect

from . import views
from django.urls import path
from django.conf.urls import url

urlpatterns = [
    path('', views.index, name="index"),
    path('aportaciones', views.aportaciones),
    path('user', views.user_page),
    path('aemet/<str:municipio>-id<int:municipio_id>', views.aportacion_aemet_page),
    path('wiki/<str:articulo>', views.aportacion_wiki_page),
    path('youtube/<str:video>', views.aportacion_youtube_page),
    path('reddit/<str:id_reddit>', views.aportacion_reddit_page),
    path('no_reconocido/<str:titulo>', views.aportacion_no_reconocido),
    path('modo_oscuro', views.modo_oscuro),
    path('modo_normal', views.modo_normal),
    path('comentarios', views.tipo_comentarios),
    path('informacion', views.informacion),
    url(r'^like/$', views.like_content, name="like_content"),
    url(r'^dislike/$', views.dislike_content, name="dislike_content"),
    url(r'^like_aportacion/$', views.like_aportacion, name="like_aportacion"),
    url(r'^dislike_aportacion/$', views.dislike_aportacion, name="dislike_aportacion")
]
