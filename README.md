# Final Lo Visto

Práctica final del curso 2020/21

## Datos
* Nombre: Emilio Castejón Toribio
* Titulación: Ingeniería en Tecnologías de Telecomunicación
* Nombre en los laboratorios: emilioct
* Video básico (url): https://www.youtube.com/watch?v=zeEM8qJU3Mo
* Video parte opcional (url): https://www.youtube.com/watch?v=sZ8GhNMtamg
* Despliegue (url): http://emilioct.pythonanywhere.com/
## Cuenta Admin Site
* admin1/admin1
* admin2/admin2
## Cuentas usuarios
* admin1/admin1
* admin2/admin2
## Resumen parte obligatoria
La parte obligatoria ha sido incluida en su totalidad, con prueba de funcionamiento en el video incluido arriba.
## Lista partes opcionales
* Inclusión de un favicon del sitio
* Visualización de cualquier página en formato JSON y/o XML, de forma
similar a como se ha indicado para la página principal.
* Generación de un documento XML y/o JSON para los comentarios puestos
en el sitio. Cada contenido con sus comentarios.
* Incorporación de datos de otros tipos de recurso además de los obligatorios. En este caso se reconoce también el recurso reddit aparte de los 3 obligatorios.
* Inclusión de imágenes (no solo texto) en los comentarios. Se ha añadido un tercer campo en los comentarios para añadir la url de la imagen que el usuario quiere poner.
